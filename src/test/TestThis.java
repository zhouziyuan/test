package test;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

public class TestThis {
	
	public static String getSignData(Map map){
		StringBuffer sb = new StringBuffer();
		//把集合放进去
		Set<Map.Entry<String, String>> entry = map.entrySet();
		//迭代器遍历
		Iterator<Map.Entry<String, String>> it = entry.iterator();
		//循环遍历
		while(it.hasNext()){
			Entry<String, String> next = it.next();
			String key = next.getKey();
			String value = next.getValue();
		    sb.append(key+"="+value+"&");
		}
		return sb.toString().substring(0, sb.length()-1);
	}
	
	public static Map<String,String> getMapData(String data){
		   Map<String,String> returnMap = new TreeMap<>();
	        String [] datas = data.split("&");
	        for (int i =0;i<datas.length;i++){
	            String [] temp = datas[i].split("=");
	            StringBuffer tempBuff = new StringBuffer();
	            for (int j=1;j<temp.length;j++){
	                tempBuff.append(temp[j]+"=");
	            }
	            if (temp.length != 1) {
	                returnMap.put(temp[0],tempBuff.toString().substring(0,tempBuff.length()-1));
	            }else{
	                returnMap.put(temp[0],"");
	            }
	        }
	        return returnMap;
	}
		
	//性别=男&名字=周子元&地址=如皋&年龄=27
	public static void main(String[] args) {
//		Map<String,String> map = new HashMap<>();
//		map.put("名字", "周子元");
//		map.put("年龄", "27");
//		map.put("性别", "男");
//		map.put("地址", "如皋");
//		System.out.println(TestThis.getSignData(map));
		String data = "性别=男&名字=周子元&地址=如皋&年龄=27";
		System.out.println(TestThis.getMapData(data));
		Map<String,String> map = new HashMap<>();
		map.put("姓名", "121");
		map.put("as", "asda");
		System.out.println(map);
	}
}
